                                                    _________________________
                                                    |                       |
                                                    |    Protein graph      |
                                                    |_______________________|    

La creación de este proyecto esta enfocada en el manejo y procesamiento de datos, se busca procesar cualquier proteína y crear una imagen a partir de la información entregada por un archivo pdb.

+ Empezando
    El programa cosiste en un generador de imagenes que inicialmente presenta un menú con 3 opciones. La primera opción es descargar las    proteinas, la cual se encarga de verificar si existe la base de datos (bd-pdb.txt), luego se verifica que el parametro ingresado sea correcto, si este es correcto se verifica que corresponda a una proteína y si es así la descarga, finalmente le da posibilidad al usuario de volver al menú para que este pueda escoger que hacer. La segunda opción permite generar las imagenes, muestra al usuario los archivos disponibles para que eliga la proteína que desea, una vez elegida se filtra la información necesaria para generar el archivo dot que luego se convertira en una imagen. Finalmente la tercera opción le permite al usuario cerrar el programa.


+ Requisitos previos
    Sistema operativo linux.
    Graphviz 
    Conexión a internet.

 
+ Instalación
    Para instalar Graphviz debe usar los comandos:
        [1] sudo apt-get install graphviz
        [2] sudo apt-get update


+ Ejecutando las pruebas por terminal
    Para la entrada al programa en el editor de texto vim, se necesita del comando: 
        [1] vim nombrearchivo.sh 

    Para la entrada de los archivos awk se usa el comando:
        [2] vim nombrearchivo.awk

    Para la entrada de los archivos dot se usa el comando:
        [3] vim nombrearchivo.dot

    Mientras que para ejecutarlo debemos colocar el comando:
        [4] bash nombrearchivo.sh parametro
        Parametro ----> Corresponde al PDB ID de la proteína. 


+ Construido con
    Sistema operativo: Ubuntu 17.10
    Vim: Editor de texto para escribir el código del programa. 
    Bash: Es un programa informático, cuya función consiste en interpretar órdenes, y un lenguaje de consola.
    Awk: Es un lenguaje de programación diseñado para procesar datos basados en texto
    Graphviz: Conjunto de herramientas de software para el diseño de diagramas definido en el lenguaje descriptivo DOT.
    Pep-8: La narración del código realizado en este proyecto esta basado en las instrucciones dadas por la pep-8.


+ Especificaciones de pep-8 y GNU code style
    Los tab utilizados en el código están designados por 4 espacios como dice que debe ser dentro de ella. Esto se puede probar dentro de   cualquier linea presente en el código.
    Despues de cada función se debe dejar dos líneas en blanco.
     Para cada comentario realizado en el código se utiliza de un # para el cual luego debe tener un espacio de separación entre # y la primera palabra escrita en el comentario. 
    Las llaves al inciar una función van en la línea siguiente a la definición.

    -Ejemplo para dar a conocer lo anteriormente escrito pero implementado en el codigo: 
        96 function base_de_datos 
        97 {
        98 
        99      # Revisar si la base de dato ya existe
        100     if [[ ! -f $archivo ]]; then
        101         clear
        102         echo "-----------Se descargara la base de datos------------"
        103         wget https://icb.utalca.cl/~fduran/bd-pdb.txt
        104         descargar_proteina $parametro $archivo
        105     else
        106         clear
        107         echo -e '\n' "La base de datos ya se encuentra en su computador" '\n'
        108         descargar_proteina $parametro $archivo
        109     fi
        110 } 
        111 
        112 
        113 function parametro_ 
        114 {
        115 
        116     largo=`echo -n $parametro |wc -c`
        117     # Revisar la cantidad de parametros ingresados
        118     # Cuando se ingresa 1
        119     if [[ $linea -eq 1 ]]; then
        120         clear
        121         # Revisa el largo del parametro (ID proteina es de largo 4)
        122         if [[ $largo -lt 4 ]]; then
        123             echo -e '\n' "FATAL: El parametro ingreado no posee la cantidad correcta de carateres"
        124             echo -e "Recuerde el parametro debe ser de largo 4, el ingresado es de largo < $largo >" '\n'
        125         else
        126             # Revisar si la base de dato ya existe
        127             base_de_datos $archivo
        128         fi




+ Versiones
    Ubuntu 17.10
    GNU bash, versión 4.4.12(1)-release (x86_64-pc-linux-gnu)


+ Autor
    Nicole Soto


+ Expresiones de gratitud
    Ejemplos subidos a GitLab https://gitlab.com/fabioduran/programacion-i/tree/master/gtk/ejemplos Por Fabio Duran



