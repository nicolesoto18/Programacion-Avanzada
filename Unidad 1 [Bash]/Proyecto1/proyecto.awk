BEGIN{
    
    comillas = "\""
    print "digraph Protein {"
    print "rankdir=LR;"
    print "size="comillas"340"comillas

}


{
    elemento = $3$2 
    residuo = $4
    cadena = $5
    elemento_final=$3$2 " [label = " comillas$3comillas " ]"

    # Compara el residuo actual con el anterior    
    if (residuo != linea_anterior){
        if (linea_anterior == null){
            print "\n"
            print "node [shape=box]"
            print residuo "[ label = " comillas "Cadena " cadena comillas "];" 
            print "\n"
            print "node [shape=circle]"
            
        }

        else{
            # Cada vez que se encuentra una cadena distinta se agrega un nodo
            if (cadena != x){
                print "\n"
                print "node [shape=box]"
                print residuo " -> " linea_anterior "[ label = " comillas "Cadena " cadena comillas "];" 
                print "\n"
                print "node [shape=circle]"
            }

            # Si no cambia la cadena se agrega un nodo circular
            else{
                print "\n"
                print "node [shape=circle]"
            }   
        }

    }

    print elemento_final
    print elemento " -> " residuo
    linea_anterior = residuo
    x = cadena

}


END{
    print "}"
}

