
{   
    elem_residuo = $3
    res_cadena = $4

    # Compara el largo de las columnas que estan unidas (elemento y residuo)
    if (length(elem_residuo) > 3){
        # substr(variable que se acortara, cantidad que se acortara, cantidad de letras que quedaran)
        residuo = substr(elem_residuo,1,3)
        elemento = substr(elem_residuo,5,3) 
        print $1,"\t" $2, "\t" residuo, "\t" elemento, "\t" $4
    }

    # Compara cuando se une el residuo y la cadena
    else if (length(res_cadena) > 3){
        residuo =  substr($4,2,3)
        cadena = substr($4,0,2)
        print $1, "\t" $2, "\t" $3, "\t" residuo, "\t" cadena
   } 

    # Cuando no hay columnas unidas
    else if ((length(res_residuo) <= 3) && (length(res_cadena) <= 3)){
        print $1, "\t" $2, "\t" $3, "\t" $4, "\t" $5
    }
}


