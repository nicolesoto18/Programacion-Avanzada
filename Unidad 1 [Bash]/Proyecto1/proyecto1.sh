#!/bin/bash


function analisis_atom 
{
    filtro1= grep ^"ATOM" "$1.pdb" > "atomos.txt"
    cd ..
    if [[ ! -f  "atomos_$1.txt" ]]; then
        cat $carpeta/"atomos.txt" | awk -f Arregla_delimitador.awk > "atomos_$1.txt"
        rm $carpeta/"atomos.txt"
        clear
        echo -e '\n' '\t'"------------Se procesaran los datos para crear una imagen-----------"
        cat "atomos_$1.txt" | awk -f proyecto.awk >> $1.dot
        dot -Tpng -o "$1.png" "$1.dot"
        echo -e '\t''\t''\t''\t'"$1.png" "Se a creado con exito " '\n' 
      
    else
        rm $carpeta/"atomos.txt"
        clear
        if [[ ! -f  "$1.png" ]]; then
            echo -e '\n' '\t'"------------Se procesaran los datos para crear una imagen-----------"
            cat "atomos_$1.txt" | awk -f proyecto.awk >> $1.dot
            dot -Tpng -o "$1.png" "$1.dot"
            echo -e '\t''\t''\t''\t'"$1.png" "Se a creado con exito "'\n' 
        else
            echo -e '\n''\t'"_____________________________________________"
            echo -e '\t'"       EL ARCHIVO "$1.png" YA EXISTE"
            echo -e '\t'"_____________________________________________"
            sleep 3
            clear
        fi
    fi

}


function descargar_proteina 
{
    carpeta="./Descargas_PDB/"
    if [[ ! -d "$carpeta" ]]; then
          mkdir "$carpeta"
      fi

      fila=$(grep $parametro $archivo)
      delimitador="~"

      # Cambia el delimitador por ~ 
      fila="${fila//\"\,\"/$delimitador}"

      # Elimina las comillas
      fila="${fila//\"/}"

      while IFS= read -ra columna
      do
        # Convertir fila en un arreglo
        IFS="$delimitador" read -ra columna <<< "$fila"

        # Verifica la existencia del parametro en la base de datos
        if [[ $parametro == "${columna[0]}" ]]; then
             #Verificar si el parametro corresponde a una proteina
             if [[ "${columna[3]}" == "Protein" ]]; then
                if [[ ! -f "$carpeta/${columna[0]}.pdb" ]]; then
                    echo -e '\t'"-----------"'\t'"Se descargara la proteina ingresada"'\t'"-----------" '\n'
                    echo -e '\t''\t''\t''\t'"${columna[0]} -> ${columna[1]}"
                    # Descargar la proteina
                    wget "https://files.rcsb.org/download/${columna[0]}.pdb" -P "$carpeta"
                    echo -e '\t'"${columna[0]} -> ${columna[1]} Se ha descargado con exito" '\n'
                    sleep 2
                else 
                    echo -e '\t'"${columna[0]} -> ${columna[1]} Ya se encuentra en su sistema" '\n'
                    sleep 4
                fi
            else
                echo -e '\n'"${columna[0]} NO es una PROTEINA (Es <${columna[3]}> y NO sera descargado)" '\n'
            fi
        elif [[ $1 != "${columna[0]}" ]]; then
            echo "El parametro no existe en la base de datos"
        fi
    done <<< "$archivo"
    clear

    echo -e "¿Desea volver al menu principal?"'\n'"[1] Si"'\n'"[2] No"
    read opcion
    if [[ $opcion -eq 1 ]]; then
        clear
        menu
    elif [[ $opcion -eq 2 ]]; then
        clear
        exit
    fi
}


function base_de_datos 
{
    # Revisar si la base de dato ya existe
    if [[ ! -f $archivo ]]; then
        clear
        echo "-----------Se descargara la base de datos------------"
        wget https://icb.utalca.cl/~fduran/bd-pdb.txt
        descargar_proteina $parametro $archivo
    else 
        clear
        echo -e '\n' "La base de datos ya se encuentra en su computador" '\n'
        descargar_proteina $parametro $archivo
    fi
}       


function parametro_ 
{
largo=`echo -n $parametro |wc -c`
    # Revisar la cantidad de parametros ingresados
    # Cuando se ingresa 1
    if [[ $linea -eq 1 ]]; then
        clear
        # Revisa el largo del parametro (ID proteina es de largo 4)
        if [[ $largo -lt 4 ]]; then
            echo -e '\n' "FATAL: El parametro ingreado no posee la cantidad correcta de carateres"
            echo -e "Recuerde el parametro debe ser de largo 4, el ingresado es de largo < $largo >" '\n'
        else
            # Revisar si la base de dato ya existe
            base_de_datos $archivo
        fi

    # Cuando no se ha ingresado un parametro
    elif [[ $linea -eq "" ]]; then
        clear
        echo "No ha ingresado un parametro"

    # Cuando es mas de un parametro  
    else
        clear
        echo -e '\n'"¡SE HA PRODUCIDO UN ERROR!"
        echo "FATAL: Ha ingresado $linea parametros"
    fi

}


function menu 
{
    carpeta="./Descargas_PDB/"

    echo -e '\n' '\t' "--------------------------------"
    echo -e '\t' '\t' "PROTEIN GRAPHIC"
    echo -e '\t' "--------------------------------" '\n' 

    echo -e '\t' "[1] Descargar proteinas"
    echo -e '\t' "[2] Crear imagen"
    echo -e '\t' "[3] Salir"

    read opcion
    # Descargara proteinas ingresadas por parametro
    if [[ $opcion -eq 1 ]]; then
        parametro_ $archivo $parametro

    # Generador de imagenes
    elif [[ $opcion -eq 2 ]]; then
        if [[ -d $carpeta ]]; then
            cd $carpeta
            clear
            echo -e '\n'"Las proteinas existentes son:"
            find -name "*.pdb"
            echo "___________________________________"
            echo -e '\n'"Ingrese el PDB ID de la proteina: "
            read ID
            if [[ -f "${ID^^}.pdb" ]]; then
                analisis_atom ${ID^^}
            else 
                clear
                echo -e '\n'"FATAL: No existe el archivo" $ID
            fi
        else 
            clear
            echo -e '\t'"_________________________________________"
            echo -e '\n''\t'"    No hay proteinas descargadas"
            echo -e '\t''\t'"Volvera al menu"
            echo -e '\t'"________________________________________"
            sleep 4
            clear
            menu
        fi
    elif [[ $opcion -eq 3 ]]; then
        clear
        exit
    fi

}


archivo=bd-pdb.txt
ingreso=$1
# El parametro ingreara en mayuscula
parametro=${ingreso^^}
linea="$#"
menu 
