#!bin/bash
directorio=$1

function sin_parametro {
    echo >&2 "$@"
    exit 1
}

if [[ "$#" -ne 1 ]]; then
    echo "No ha ingresado un parametro"
fi

if [[ "$#" -eq 1 ]]; then
    ls -lSr /$HOME/$directorio | awk -F " " '{print $9}' | nl 
fi




