#!bin/bash

nombre=$1
cd --
mkdir $nombre

if [[ $nombre != -d ]]; then
    echo "La carpeta ya existe"
    echo "Ingrese un nuevo nombre para la carpeta: "
    read nuevo_nom

    mv $nombre $nuevo_nom
fi

