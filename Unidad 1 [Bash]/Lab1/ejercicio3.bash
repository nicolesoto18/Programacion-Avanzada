#/!bin/bash
fecha=`date +%d/%m/%Y`
dia=`date +%d`
mes=`date +%m`
anio=`date +%Y`

if [[ $# > 1 ]]; then
   echo "Solo admite un parametro"
   exit 1
fi

if [[ "-l" == $1 || "--long" == $1 ]]; then
    echo "El dia es $dia, el mes es $mes y el año es $anio"
fi

if [[ "-s" == $1 || "--short" == $1 ]]; then 
   echo $fecha
fi

if [[ $# != 1 ]]; then
    cal
fi

