BEGIN{

    comillas = "\""
    print "digraph Protein {"
    print "rankdir=LR;"
    #print "size="comillas"340"comillas
    flag = 1
}


{
    coor_x = $7
    coor_y = $8
    coor_z = $9
    residuo = $4$6$5
    cadena = $5
    elemento = $3$2

    distancia = sqrt(((coor_x  - x)^2) + ((coor_y  - y)^2) + ((coor_z  - z)^2))
    # distancia usuario > distancia awk 
    if (dis > distancia){
        if (flag == 1){
            flag = 0
        }
        else{
            if (residuo != residuo_anterior){
                print "\n"
                print "node [shape = box]"
                print nombre " -> " residuo
                print "\n"

                if (cadena != x){
                    print "\n"
                    print residuo " -> " residuo_anterior "[ label = " comillas "Cadena " cadena comillas "];" 
                    print "\n"
                }

                else {
                    print "\n"
                    print "node [shape = circle]"
                    print "\n"

                }

                print elemento "[label = " comillas $3 comillas "]"
                print residuo " -> " elemento
            }

        }

    residuo_anterior = residuo
    x = cadena
    }
         
}


END{
    print "}"
}

