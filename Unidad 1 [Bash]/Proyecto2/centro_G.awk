
BEGIN {
    flag = 1
 }
    

{
    # Filtro HETATM
    residuo_H = $4
    num_residuo_H = $6
    coor_x_H = $7
    coor_y_H = $8
    coor_z_H = $9

    # Primer ingreso
    if (flag == 1){
        if (((length(residuo_H) > 2))){
            suma_x += coor_x_H
            suma_y += coor_y_H 
            suma_z += coor_z_H
       
            total += 1
            }
        flag = 0

        } 

    else {
        # Corresponde a un ion
        if ((linea_anterior != num_residuo_H) && ((length(residuo_H) <= 2))){
            centro_x = coor_x_H
            centro_y = coor_y_H
            centro_z = coor_z_H
            print "ION" "\t" $4 "\t" centro_x "\t" centro_y "\t" centro_z > "CG_"$4$5$6
       
        }

        # Corresponde a un ligando
        else{
            # Hay un cambio de ligando
            if (linea_anterior != num_residuo_H){
                centro_x = suma_x / total
                centro_y = suma_y / total
                centro_z = suma_z / total
                print "LIGANDO \t" $4linea_anterior$5 "\t" centro_x "\t" centro_y "\t" centro_z "\t" > "CG_"$4linea_anterior$5

                suma_x = coor_x_H
                suma_y = coor_y_H
                suma_z = coor_z_H
                total = 1
                }

            else {
                suma_x += coor_x_H
                suma_y += coor_y_H 
                suma_z += coor_z_H
                total += 1
                }
            }

        }

    linea_anterior = $6
}


END{
    centro_x = suma_x / total
    centro_y = suma_y / total
    centro_z = suma_z / total
    
    print "LIGANDO \t" $4$6$5 "\t" centro_x "\t" centro_y "\t" centro_z > "CG_"$4linea_anterior$5
}
