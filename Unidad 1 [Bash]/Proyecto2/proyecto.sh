#!/bin/bash

function analisis_proteina
{
    carpeta="./Descargas_PDB/"
    carpeta2="./Centro_Geometrico/"
    parametro=$1

    echo "Ingrese la distancia de los elementos"
    read distancia
    filtro1= grep ^"ATOM" "$parametro.pdb" > atomos.txt
    filtro2= grep ^"HETATM" "$parametro.pdb" > temporal.txt

    cd ..
    # Elimina los HOH del archivo
    sed '/HOH/d' ./$carpeta/temporal.txt > hetatm.txt

    # Reordena el archivo
    cat $carpeta/"atomos.txt" | awk -f Arregla_delimitador.awk > "$parametro _ATOM.txt"
    cat "hetatm.txt" | awk -f Arregla_delimitador.awk > "$parametro _HETATM.txt"

    # Eliminar archivos temporales
    rm $carpeta/"atomos.txt"
    rm $carpeta/"temporal.txt"
    rm "hetatm.txt"
    
    awk -f centro_G.awk "$parametro _HETATM.txt"
    rm "$parametro _HETATM.txt"
    clear

    # Recorre los archivos que contienen el centro geometrico
    for i in $(ls | grep ^"CG");
    do
        if [[ ! -f "$parametro$i.svg" ]]; then
            nom=$(awk '{print $2}' $i)
            c_x=$(awk '{print $3}' $i)
            c_y=$(awk '{print $4}' $i)
            c_z=$(awk '{print $5}' $i)
            # Calculara la distancia 3D y generara un grafo para cada elemento
            awk -v x="$c_x" -v y="$c_y" -v z="$c_z" -v dis=$distancia -v nombre=$nom -f dis3D.awk "$parametro _ATOM.txt" > "graf_$i.dot"
            echo -e '\n' "Se procesaran los datos"
            sleep 5
            dot -Tsvg -o "$parametro$i.svg" "graf_$i.dot"
            rm $i
            rm "graf_$i.dot"
            echo -e '\n' "La imagen de $i se han creado con exito"
            sleep 5


        else
           clear
           echo -e '\t' "Las imagenes de $parametro ya se encuentran en el sistema" "\t"
           sleep 10
           menu
       fi
       
    done

    rm "$parametro _ATOM.txt"

    echo -e "¿Desea volver al menu principal?"'\n'"[1] Si"'\n'"[2] No"
    read opcion
    if [[ $opcion -eq 1 ]]; then
        clear
        menu
    elif [[ $opcion -eq 2 ]]; then
        clear
        exit
    fi

}


function descargar_proteina 
{
    parametro=$1
    carpeta="./Descargas_PDB/"

    if [[ ! -d "$carpeta" ]]; then
          mkdir "$carpeta"
      fi

      fila=$(grep $parametro $archivo)
      delimitador="~"

      # Cambia el delimitador por ~ 
      fila="${fila//\"\,\"/$delimitador}"

      # Elimina las comillas
      fila="${fila//\"/}"

      while IFS= read -ra columna
      do
        # Convertir fila en un arreglo
        IFS="$delimitador" read -ra columna <<< "$fila"

        # Verifica la existencia del parametro en la base de datos
        if [[ $parametro == "${columna[0]}" ]]; then
             #Verificar si el parametro corresponde a una proteina
             if [[ "${columna[3]}" == "Protein" ]]; then
                if [[ ! -f "$carpeta/${columna[0]}.pdb" ]]; then
                    echo -e '\t'"-----------"'\t'"Se descargara la proteina ingresada"'\t'"-----------" '\n'
                    echo -e '\t''\t''\t''\t'"${columna[0]} -> ${columna[1]}"
                    # Descargar la proteina
                    wget "https://files.rcsb.org/download/${columna[0]}.pdb" -P "$carpeta"
                    echo -e '\t'"${columna[0]} -> ${columna[1]} Se ha descargado con exito" '\n'
                    sleep 4
                else 
                    echo -e '\t'"${columna[0]} -> ${columna[1]} Ya se encuentra en su sistema" '\n'
                    sleep 4
                fi
            else
                echo -e '\n'"${columna[0]} NO es una PROTEINA (Es <${columna[3]}> y NO sera descargado)" '\n'
                sleep 4
            fi
        elif [[ $parametro != "${columna[0]}" ]]; then
            echo "El parametro no existe en la base de datos"
            sleep 4
        fi
    done <<< "$archivo"
    clear

    echo -e "¿Desea volver al menu principal?"'\n'"[1] Si"'\n'"[2] No"
    read opcion
    if [[ $opcion -eq 1 ]]; then
        clear
        menu
    elif [[ $opcion -eq 2 ]]; then
        clear
        exit
    fi
}


function base_de_datos 
{
    # Revisar si la base de dato ya existe
    if [[ ! -f $archivo ]]; then
        clear
        echo "-----------Se descargara la base de datos------------"
        wget https://icb.utalca.cl/~fduran/bd-pdb.txt
        descargar_proteina $1 $archivo
    else 
        clear
        echo -e '\n' "La base de datos ya se encuentra en su computador" '\n'
        descargar_proteina $1 $archivo
    fi
}       


function parametro_ 
{
    largo=`echo -n $1 |wc -c`
    # Revisa el largo del parametro (ID proteina es de largo 4)
    if [[ $largo -lt 4 ]]; then
        clear
        echo -e '\n' "FATAL: El parametro ingreado no posee la cantidad correcta de carateres"
        echo -e "Recuerde el parametro debe ser de largo 4, el ingresado es de largo < $largo >" '\n'
        sleep 10
        menu

    else
        # Revisar si la base de dato ya existe
        base_de_datos $1 $archivo
    fi

}


function menu 
{

    carpeta="./Descargas_PDB/"
    clear
    echo -e '\n' '\t' "--------------------------------"
    echo -e '\t' '\t' "PROTEIN GRAPHIC"
    echo -e '\t' '\t' "      2.1"
    echo -e '\t' "--------------------------------" '\n' 

    echo -e '\t' "[1] Descargar proteinas"
    echo -e '\t' "[2] Crear imagen"
    echo -e '\t' "[3] Ingresar una nueva proteína"
    echo -e '\t' "[4] Salir"

    read opcion

    # Descargara proteinas ingresadas por parametro
    if [[ $opcion -eq 1 ]]; then
        clear
        echo -e '\n'"Ingrese el PDB ID de la proteina: "
        read ingreso

        # El parametro ingreara en mayuscula
        parametro=${ingreso^^}
        parametro_ $parametro $archivo

    # Generador de imagenes
    elif [[ $opcion -eq 2 ]]; then
        if [[ -d $carpeta ]]; then
            cd $carpeta
            clear
            echo -e '\n'"Las proteinas existentes son:"
            find -name "*.pdb"
            echo "___________________________________"
            echo -e '\n'"Ingrese el PDB ID de la proteina: "
            read ID

            if [[ -f "${ID^^}.pdb" ]]; then
                analisis_proteina ${ID^^}
            else 
                clear
                echo -e '\n'"No existe el archivo" ${ID^^}".pdb"
                echo -e '\n'"¿Desea descargar el archivo? [S/N]"
                read opcion
                if [[ ${opcion^^} == "S" ]]; then
                    cd ..
                    parametro_ ${ID^^} $archivo
                else 
                    menu
                fi
            fi

        else 
            clear
            echo -e '\t'"_________________________________________"
            echo -e '\n''\t'"    No hay proteinas descargadas"
            echo -e '\t''\t'"Volvera al menu"
            echo -e '\t'"________________________________________"
            sleep 4
            clear
            menu
        fi
    elif [[ $opcion -eq 3 ]]; then 
        clear
        echo -e '\n'"Ingrese el PDB ID de la proteina: "
        read ID
        if [[ -d $carpeta ]]; then
            cd $carpeta
            if [[ -f "${ID^^}.pdb" ]]; then
                echo -e '\n'"El archivo"  ${ID^^}".pdb ya se encuentra en el sistema"
                echo -e '\n'"¿Desea volver al menu? [S/N]"
                read opcion
                if [[ ${opcion^^} == "S" ]]; then
                    clear 
                    menu
                else
                    clear
                    exit
                fi
            else
                clear
                echo -e '\n'"No existe el archivo" ${ID^^}".pdb"
                echo -e '\n'"¿Desea descargar el archivo? [S/N]"
                read opcion
                if [[ ${opcion^^} == "S" ]]; then
                    cd ..
                    parametro_ ${ID^^} $archivo
                else
                    clear 
                    menu
                fi
            fi
        else 
            ls -1
        fi
            
    elif [[ $opcion -eq 4 ]]; then
        clear
        exit
    fi

}


archivo=bd-pdb.txt
menu $archivo 
