                                                    _________________________
                                                    |                       |
                                                    |    Protein graph      |
                                                    |         2.1           |
                                                    |_______________________|    
                                                  

La creación de este proyecto esta enfocada en el manejo y procesamiento de datos, se busca procesar cualquier proteína, calcular el centro geométrico de sus componetes y crear una imagen con los atomos y compontes que esten dentro de un rango establecido.

+ Empezando
    El programa consiste en un generador de imagenes representativas de los componentes de una proteína, es decir se realizaran una imagen por cada ligando o ion que presente la proteína.
    Inicialmente se presenta un menú con 4 opciones, la primera es descargar las proteinas, la cual se encarga de pedir una proteína, verificar si existe la base de datos (bd-pdb.txt), luego se verifica que el parametro ingresado sea correcto, si este es correcto se verifica que corresponda a una proteína y si es así la descarga, finalmente le da posibilidad al usuario de volver al menú para que este pueda escoger que hacer. La segunda opción permite generar las imagenes, muestra al usuario los archivos disponibles para que eliga la proteína que desea, una vez elegida se filtra la información necesaria para generar el archivo dot que luego se convertira en una imagen esta debe ser abierta con un navegador de internet. La tercera opción le permite al usuario cambiar el PDB ID que fue ingresado por lo tanto puede interactuar con una proteína distinta, la ejecución de esta opción no es 100% correcta. Finalmente la cuarta opción es la salida del programa.


+ Requisitos previos
    Sistema operativo linux.
    Graphviz 
    Conexión a internet.

 
+ Instalación
    Para instalar Graphviz debe usar los comandos:
        [1] sudo apt-get install graphviz
        [2] sudo apt-get update


+ Ejecutando las pruebas por terminal
    Para ejecutar debemos colocar el comando:
        [1] bash nombrearchivo.sh
        
    Para la entrada al programa en el editor de texto vim, se necesita del comando: 
        [2] vim nombrearchivo.sh 

    Para la entrada de los archivos awk se usa el comando:
        [3] vim nombrearchivo.awk

    Para la entrada de los archivos dot se usa el comando:
        [4] vim nombrearchivo.dot

     


+ Construido con
    Sistema operativo: Ubuntu 17.10
    Vim: Editor de texto para escribir el código del programa. 
    Bash: Es un programa informático, cuya función consiste en interpretar órdenes, y un lenguaje de consola.
    Awk: Es un lenguaje de programación diseñado para procesar datos basados en texto
    Graphviz: Conjunto de herramientas de software para el diseño de diagramas definido en el lenguaje descriptivo DOT.
    Pep-8: La narración del código realizado en este proyecto esta basado en las instrucciones dadas por la pep-8.


+ Especificaciones de pep-8 y GNU code style
    Los tab utilizados en el código están designados por 4 espacios como dice que debe ser dentro de ella. Esto se puede probar dentro de   cualquier linea presente en el código.
    Despues de cada función se debe dejar dos líneas en blanco.
     Para cada comentario realizado en el código se utiliza de un # para el cual luego debe tener un espacio de separación entre # y la primera palabra escrita en el comentario. 
    Las llaves al inciar una función van en la línea siguiente a la definición.

    -Ejemplo para dar a conocer lo anteriormente escrito pero implementado en el codigo: 
       function base_de_datos 
       {
            # Revisar si la base de dato ya existe
            if [[ ! -f $archivo ]]; then
                clear
                echo "-----------Se descargara la base de datos------------"
                wget https://icb.utalca.cl/~fduran/bd-pdb.txt
                descargar_proteina $1 $archivo
            else 
                clear
                echo -e '\n' "La base de datos ya se encuentra en su computador" '\n'
                descargar_proteina $1 $archivo
            fi
        }       


        function parametro_ 
        {
            largo=`echo -n $1 |wc -c`
            # Revisa el largo del parametro (ID proteina es de largo 4)
            if [[ $largo -lt 4 ]]; then
                clear
                echo -e '\n' "FATAL: El parametro ingreado no posee la cantidad correcta de carateres"
                echo -e "Recuerde el parametro debe ser de largo 4, el ingresado es de largo < $largo >" '\n'
                sleep 10
                menu

            else
                # Revisar si la base de dato ya existe
                base_de_datos $1 $archivo
            fi

        }


+ Versiones
    Ubuntu 17.10
    GNU bash, versión 4.4.12(1)-release (x86_64-pc-linux-gnu)


+ Autor
    Nicole Soto


+ Expresiones de gratitud
    Ejemplos subidos a GitLab https://gitlab.com/fabioduran/programacion-i/tree/master/gtk/ejemplos Por Fabio Duran



