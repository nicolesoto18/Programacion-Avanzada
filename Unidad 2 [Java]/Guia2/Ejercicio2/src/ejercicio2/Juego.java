package ejercicio2;

public class Juego {
	Persona jugador1;
	Persona jugador2;
	public  int puntos1 = 0;
	public 	int puntos2 = 0;
	
	
	Juego(){
		jugador1 = new Persona();	
		jugador2 = new Persona();
	}
	
	public void tie_break() {
		
		if (jugador1.lanzamiento.contentEquals("W") && puntos1 == 0 && puntos2 == 0) {	
			ganar(jugador1, puntos2, puntos1);
			
		}
	}
	
	
	
	public void ganar(Persona jugador, int var1, int var2) {
		if (jugador.tipo_tiro == 1) {
			System.out.println("La pelota cayo en la cancha, el juego continua");	
		}
		
		else if(jugador.tipo_tiro == 2) {
			System.out.println("La pelota se quedo en la red, pierde el punto");
			var2++;		
			}
		
		else if(jugador.tipo_tiro == 3) {
			System.out.println("La pelota cayó fuera de la cancha, pierde el punto");
			var2++;
		}
		
		else if(jugador.tipo_tiro == 4) {
			System.out.println("¡¡TIRO GANADOR!!\nSumas un punto " + jugador.getNombre());
			var1++;
		}
	}
	
}

