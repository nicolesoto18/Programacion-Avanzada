package ejercicio2;
import java.util.Scanner;

public class Persona {
	public String direccion;
	private int energia;
	public String lanzamiento;
	private String nombre;
	public int tipo_tiro;

	public void primer_juego() {
		Scanner entrada = new Scanner(System.in);
		
		energia = (int) (Math.random() * 10 ) + 5;
		setEnergia(energia);
		tipo_tiro = (int) (Math.random() * 4 ) + 1;
		/* 1: Dentro de la cancha
		 * 2: Pelota en red
		 * 3: Fuera cancha
		 * 4: Tiro Ganador
		 */
		
		System.out.println("Ingrese su nombre");
		nombre = entrada.nextLine();
		setNombre(nombre);
		
		System.out.println("¿Cuál es la dirección del lanzamiento?\n [A] Izquierda\n [D] Derecha\n [S] Centro");
		direccion = entrada.nextLine();
		
		System.out.println("Para lanzar presione [ W ]");
		lanzamiento = entrada.nextLine();
		
		entrada.close();
	}
	
	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getEnergia() {
		return energia;
	}

	public void setEnergia(int energia) {
		this.energia = energia;
	}
	

}
