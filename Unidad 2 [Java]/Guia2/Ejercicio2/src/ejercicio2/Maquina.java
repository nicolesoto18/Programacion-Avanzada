package ejercicio2;
public class Maquina {
	private char direccion;
	public int energia;
	public int tipo_tiro;



	public void primer_juego() {
		char letra;
		energia = (int) (Math.random() * 10 ) + 5;
		tipo_tiro = (int) (Math.random() * 4 ) + 1;
		/* 1: Dentro de la cancha
		 * 2: Pelota en red
		 * 3: Fuera cancha
		 * 4: Tiro Ganador
		 */
		
		letra = (char)(Math.random()*26+65);
		if (letra == 'A' || letra == 'S' || letra == 'D') {
			setDireccion(letra);
		}
		else {
			primer_juego();
		}
		
	}


	public char getDireccion() {
		return direccion;
	}

	public void setDireccion(char direccion) {
		this.direccion = direccion;
	}


}
