package ejercicio1;

public class Juego_Doble {
	public int numero1;
	public int numero2;
	public boolean contador  = true;
	
	
	public void	 ganar(){
		while(contador) {
			System.out.println("JUGADOR 1");
			Persona persona1 = new Persona();
			persona1.doble();
			
			System.out.println("JUGADOR 2");
			Persona persona2 = new Persona();
			persona2.doble();
			
			if (persona1.getJugada_der() == 1 && persona2.getJugada_der() == 3 || persona1.getJugada_izq() == 1 && persona2.getJugada_izq() == 3){
				contador = false;
				System.out.println("Ganador " + persona1.getNombre());
				System.out.println("Brazos arriba" + persona1.getNombre());
			}
			
			else if (persona1.getJugada_der() == 2 && persona2.getJugada_der() == 1 || persona1.getJugada_izq() == 2 && persona2.getJugada_izq() == 1){
				contador = false;
				System.out.println("Ganador " + persona1.getNombre());
				System.out.println("Brazos arriba" + persona1.getNombre());
			}
			
			else if (persona1.getJugada_der() == 3 && persona2.getJugada_der() == 2 || persona1.getJugada_izq() == 3 && persona2.getJugada_izq() == 2){
				contador = false;
				System.out.println("Ganador " + persona1.getNombre());
				System.out.println("Brazos arriba" + persona1.getNombre());
			}
			
			else if (persona1.getJugada_der() == persona2.getJugada_der() || persona1.getJugada_izq() == persona2.getJugada_izq()){
				contador = true;
				System.out.println("Empate");
			}
			else {
				contador = false;
				System.out.println("Ganador " + persona2.getNombre());
				System.out.println("Brazos arriba " + persona2.getNombre());
			}
		}
	
	}
}
