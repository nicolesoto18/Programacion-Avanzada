package ejercicio1;

public class Mano {
	private int lanzamiento;

	Mano() {
		System.out.println(" [1] Piedra\n [2] Papel\n [3] Tijera");
		
	}
	
	public int getLanzamiento() {
		return lanzamiento;
	}

	public void setLanzamiento(int lanzamiento) {
		this.lanzamiento = lanzamiento;
	}

}
