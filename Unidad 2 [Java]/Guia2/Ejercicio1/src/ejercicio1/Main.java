package ejercicio1;
import java.util.Scanner;

public class Main {
	public static int tipo;
	
	public static void main(String[] args) {
		
		Scanner entrada = new Scanner(System.in);
		
		System.out.println("Bienvenido a Piedra, papel o tijera");
		System.out.println("Seleccione el tipo de juego\n [1] Juego Simple\n [2] JUego Doble");
		tipo = entrada.nextInt();
		
		if (tipo == 1) {
			Juego_Simple inicio = new Juego_Simple();
			inicio.ganar();
		}
		else if (tipo == 2) {
			Juego_Doble inicio = new Juego_Doble();
			inicio.ganar();
		}
	
		entrada.close();

	}

}
