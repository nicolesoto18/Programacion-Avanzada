package ejercicio1;
import java.util.Scanner;

public class Persona {
	private int eleccion;
	private int jugada;
	private int jugada_der;
	private int jugada_izq;
	private String nombre;
	
	Scanner entrada = new Scanner(System.in);
	
	
	public void simple() {
		System.out.println("Ingrese su nombre");
		nombre = entrada.nextLine();

		System.out.println("¿Con qué mano jugara?\n [1] Izquierda\n [2] Derecha");
		eleccion = entrada.nextInt();
		
		if (eleccion == 1) {
			Mano izquierda = new Mano();
			jugada = entrada.nextInt();
			izquierda.setLanzamiento(jugada);
		}
		
		else if (eleccion == 2) {
			Mano derecha = new Mano();
			jugada = entrada.nextInt();
			derecha.setLanzamiento(jugada);
		}
		
			
	}

	public void doble() {
		System.out.println("Ingrese su nombre");
		nombre = entrada.nextLine();
		
		Mano derecha = new Mano();
		jugada_der = entrada.nextInt();
		derecha.setLanzamiento(jugada_der);
		
		Mano izquierda = new Mano();
		jugada_izq = entrada.nextInt();
		izquierda.setLanzamiento(jugada_izq);
	}
	
	public int getEleccion() {
		return eleccion;
	}

	public void setEleccion(int eleccion) {
		this.eleccion = eleccion;
	}


	public int getJugada() {
		return jugada;
	}


	public void setJugada(int jugada) {
		this.jugada = jugada;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getJugada_der() {
		return jugada_der;
	}

	public void setJugada_der(int jugada_der) {
		this.jugada_der = jugada_der;
	}

	public int getJugada_izq() {
		return jugada_izq;
	}

	public void setJugada_izq(int jugada_izq) {
		this.jugada_izq = jugada_izq;
	}
	
	
}
