public class Estanque {
	private int tipo_combustible;
	private int cantidad_combustible = 100; 
	
	// Random para determinar tipo de combustible aleatoriamente
	public void combustible() {
		tipo_combustible = (int) (Math.random() * 3 ) + 1; 
		/* Si es 1: combustible nuclear.
		 * Si es 2: combustible diesel.
		 * Si es 3: combustible biodiesel.*/
	}
	
	// Getter  Setters
	public void deterioro_combustible() {
		cantidad_combustible -= 40;
	} 

	public int getTipo_combustible() {
		return tipo_combustible;
	}

	public void setTipo_combustible(int tipo_combustible) {
		this.tipo_combustible = tipo_combustible;
	}

	public int getCantidad_combustible() {
		return cantidad_combustible;
	}

	public void setCantidad_combustible(int cantidad_combustible) {
		this.cantidad_combustible = cantidad_combustible;
	}
}
