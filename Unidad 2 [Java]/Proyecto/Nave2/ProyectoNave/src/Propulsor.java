public class Propulsor {	
	private double aceleracion;
	public int cambio_velocidad;
	int cambio_tiempo;
	
	public void calculo_aceleracion(int vel_inicial, int vel_final, int t_inicial, int t_final, int combustible) {
		
		cambio_velocidad = vel_final - vel_inicial;
		cambio_tiempo = t_final - t_inicial;
		
		if (cambio_tiempo == 0) {
			cambio_tiempo = t_final + t_inicial;
		}
		
		aceleracion = (cambio_velocidad) / (cambio_tiempo);
		
		if(aceleracion < 0) {
			aceleracion = aceleracion * (-1);
		}
		
		aceleracion_combustible(combustible);
	}
	
	public void aceleracion_combustible(int combustible) {
		
		// Se agrega porcentaje extra de aceleración dependiendo del combustible.
		if (combustible == 1) {
			//System.out.println("Combustible Nuclear ");
			aceleracion = aceleracion * 1.04; 
			setAceleracion(aceleracion);
		}
	
		else if (combustible == 2) {
			//System.out.println("Combustible Diesel ");
			aceleracion = aceleracion * 1.02;
			setAceleracion(aceleracion);
		}
		
		else {
			//System.out.println("Combustible Biodiesel ");
			aceleracion = aceleracion * 1.003;
			setAceleracion(aceleracion);
		}
	}
	
	public double getAceleracion() {
		return aceleracion;
	}

	public void setAceleracion(double aceleracion) {
		this.aceleracion = aceleracion;
	}
}
