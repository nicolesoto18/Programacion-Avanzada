public class Nave  implements InterfazNave, Comparable<Nave> {
	// Creación de objetos.
	Estanque estanque;
	Odometro odometro;
	Propulsor propulsor; 
	Alas alas; 
	
	// Variables
	private int deterioro = 100;
	private int nombre;
	private int tiempo_actual = 0;
	private	int velocidad_actual = 0;
	private int distancia_actual = 0;
	private int estado_nave;
	private String color;
	
	Nave(){
		alas = new Alas();
		estanque = new Estanque();
		odometro = new Odometro();
	    propulsor = new Propulsor();
	}
	
	public void ensamble() {
		estanque.combustible();
		odometro.tiempo_aleatorio(1,30);
	}
		
	public void calculo() {
		int distancia;
		
		// Dar color a las naves contrincantes.
		if (alas.getColor_alas() == 0) {
			alas.verificar_color();
			crear_nombre(alas.getColor_alas());
		}
		else {
			crear_nombre(alas.getColor_alas());
		}
		
		// Determina movimiento de la nave.
		if (velocidad_actual < 1000) {
			velocidad_actual += propulsor.getAceleracion();
		}
		// La nave alcanza su velocidad máxima.
		else {
			velocidad_actual = 1000;
			propulsor.setAceleracion(0);
		}
	 	  	    
	    propulsor.calculo_aceleracion(velocidad_actual, 1000, tiempo_actual,odometro.getTiempo(), estanque.getTipo_combustible());
	    odometro.calculo_distancia(getDistancia_actual());
	
	    distancia = getTiempo_actual() * getVelocidad_actual();
	    distancia_actual += distancia;
	    setDistancia_actual(distancia_actual);
	    
	    tiempo_actual += odometro.getTiempo();
	    setTiempo_actual(tiempo_actual);
	    
	   // System.out.println("");
	    multiplo_velocidad(velocidad_actual);
	}
	
	// Por cada multiplo de 5 en la velocidad, hay deterioro.
	public void multiplo_velocidad(int velocidad_actual) { 
 		int resto;
		int numero1 = velocidad_actual;
		int numero2 = 5;
		 
		resto = numero1 % numero2;
		 
		if (resto == 0) {
			deterioro();
		}
	}
	
	// Evalúa el tipo de deterioro.
	public void deterioro() { 
		int tipo_deterioro;
		
		tipo_deterioro = (int) (Math.random() * 4) + 1;
		
		if (tipo_deterioro == 1) {
			estanque.deterioro_combustible();
			
			if(estanque.getCantidad_combustible() == 0) {
				estado_nave = 1;
				
			}

			else {
				estado_nave = 5;
				deterioro -= 30;
			}	
		}
		
		else if (tipo_deterioro == 2) {
			estado_nave = 2;
			deterioro -= 15; 
		}
		
		else if (tipo_deterioro == 3) {
			estado_nave = 3;
			deterioro -= 20;
			
		}
		
		else {
			estado_nave = 4;
			deterioro -= 25; 
		}
	}
	
	// Le da un color distinto a la nave del usuario para identificarlo. 
	public void crear_nombre(int num_color) { 
 		String verde = "\u001B[42m";
		String morado = "\u001B[45m";
		String celeste = "\u001B[46m";
		String blanco = "\u001B[37m";
		String rojo = "\u001B[31m";
		String amarillo = "\u001B[33m";
		String azul = "\u001B[34m";

		if (num_color == 1) {
			setColor(celeste);
		}
	
		else if(num_color == 2) {
			setColor(verde);
		}
		
		else if(num_color == 3) {
			setColor(morado);
		}
		
		else if(num_color == 4){
			setColor(blanco);
		}
		
		else if(num_color  == 5){
			setColor(rojo);
		}
	
		else if(num_color  == 6){
			setColor(amarillo);
		}
		
		else if(num_color  == 7){
			setColor(azul);
		}
	}
	
	 public int compareTo(Nave i) {
	        int resultado = 0;
	        if(this.tiempo_actual > i.tiempo_actual){
	        	resultado =+1;
	        }
	        else if (this.tiempo_actual <i.tiempo_actual){
	        	resultado =- 1;
	        }
	        return resultado;
	    }

	
	// Getters y Setters
	public int getNombre() {
		return nombre;
	}

	public void setNombre(int nombre) {
		this.nombre = nombre;
	}

	public int getTiempo_actual() {
		return tiempo_actual;
	}

	public void setTiempo_actual(int tiempo_actual) {
		this.tiempo_actual = tiempo_actual;
	}

	public int getVelocidad_actual() {
		return velocidad_actual;
	}

	public void setVelocidad_actual(int velocidad_actual) {
		this.velocidad_actual = velocidad_actual;
	}

	public int getDistancia_actual() {
		return distancia_actual;
	}

	public void setDistancia_actual(int distancia_actual) {
		this.distancia_actual = distancia_actual;
	}

	public int getDeterioro() {
		return deterioro;
	}

	public void setDeterioro(int deterioro) {
		this.deterioro = deterioro;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public int getEstado_nave() {
		return estado_nave;
	}

	public void setEstado_nave(int estado_nave) {
		this.estado_nave = estado_nave;
	}

}
