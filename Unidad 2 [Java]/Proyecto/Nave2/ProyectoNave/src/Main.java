import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		int opcion = 0;
		String celeste = "\u001B[36m";
		String reset = "\u001B[0m";
		Scanner entrada = new Scanner(System.in);
		
		// Menú
		System.out.print("\033[H\033[2J");
		System.out.flush();
		System.out.println("");
		System.out.println("   *    *      *    *   *  *****   *     *    *     ");
		System.out.println("   * *  *      * *  *   *  *   *    *   *    * *   ");
		System.out.println("   *  * *  **  *  * *   *  * *       * *    * * *   ");
		System.out.println("   *    *      *    *   *  *   *      *    *     *  \n");

		System.out.println( "  ------------------------------------------------");
		System.out.println(celeste + "                SUPER CARRERA N-NIRVA      " + reset);
		System.out.println( "  ------------------------------------------------" );
		System.out.println( " \n                [1] Iniciar el juego\n                [2] Salir");
		
		opcion = entrada.nextInt();
		
		// Evalúa opcion elegida por el usuario.
		if (opcion == 1) {
			System.out.print("\033[H\033[2J");
			System.out.flush();
			
			Carrera carrera = new Carrera();
			carrera.llenar_carrera();
		}
		
		else if (opcion == 2) {
			System.out.print("\033[H\033[2J");
			System.out.flush();
			System.exit(0);
		
		}
	}
}
