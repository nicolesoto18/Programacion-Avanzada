public class Locutor{
	public void inicio_carrera() {
		System.out.print("\033[H\033[2J");
		System.out.flush();
		System.out.println("La carrera comenzara en ");
		
		for (int i = 3; i > 0 ; i--) {
			System.out.print("\t" + i + "\t");
			pausa();
		}
		
		System.out.println("\n");
		System.out.print("\033[H\033[2J");
		System.out.flush();
		
		System.out.println(          "   *******   *******   *   *         ");
		System.out.println(          "   **        **    *     *  *        ");
		System.out.println(          "   ** ****   **    *      *  *       ");
		System.out.println(          "   **   **   **    *     *  *        ");
		System.out.println(          "   *******   *******    *  *         "); 
		
		pausa();
		System.out.print("\033[H\033[2J");
		System.out.flush();
		
	}
	
	public void desarrollo(int nombre, int estado_nave, int deterioro, int cantidad_combustible){
		if (estado_nave == 1) {
			System.out.println("Se acabo el combustible");
		}
		
		else if (estado_nave == 5) {
			System.out.println("El combustible de nave " + nombre + " se esta acabando, queda " + cantidad_combustible + "%");
		}
		
		else if ( estado_nave == 2) {
			System.out.println("ALERTA! Falla en el propulsor de nave " + nombre);
			System.out.println("El propulsor se dañó a un " + deterioro + "%");
		}
		
		else if(estado_nave == 3) {
			System.out.println("ALERTA! Falla en las alas de la nave " + nombre);
			System.out.println("Las alas se dañaron a un " + deterioro + "%");
		}
		
		else if(estado_nave == 4) {
			System.out.println("ALERTA! Falla en el odómetro de nave " + nombre);
			System.out.println("El odómetro se dañó a un " + deterioro + "%");
		}
		
	}
	
	public void fin_carrera() {
		System.out.print("\033[H\033[2J");
		System.out.flush();
		
		System.out.println("");
		System.out.println(          "   ******  **  *    *  **  ******  **   **	       ");
		System.out.println(          "   **      **  **   *  **  *       **   **		   ");
		System.out.println(          "   ****    **  * *  *  **  ******  *******  		   ");
		System.out.println(          "   **      **  *  * *  **       *  **   **	       ");
		System.out.println(          "   **      **  *    *  **  ******  **   **		   ");
		System.out.println("");
	}
	
	public static void pausa() {
	    try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
         
        }
	}

}
