public interface InterfazNave {
	
	public void ensamble();
	
	public void calculo();
	
	public void multiplo_velocidad(int velocidad_actual);
	
	public void deterioro();
	
	public void crear_nombre(int num_color);
	
	public int compareTo(Nave i);
	
	public int getNombre();
	
	public void setNombre(int nombre);

	public int getTiempo_actual();

	public void setTiempo_actual(int tiempo_actual);

	public int getVelocidad_actual();

	public void setVelocidad_actual(int velocidad_actual);

	public int getDistancia_actual();

	public void setDistancia_actual(int distancia_actual);

	public int getDeterioro();

	public void setDeterioro(int deterioro);

	public String getColor();

	public void setColor(String color);
	public int getEstado_nave();

	public void setEstado_nave(int estado_nave);
}
