import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;
import java.util.Map;
import java.util.Scanner;

public class Carrera {
	Nave nave;
	Ambulancia ambulancia;
	Locutor locutor;
	Scanner entrada = new Scanner(System.in);
	
	// ArrayList para agregar n naves.
	private ArrayList<Nave> naves = new ArrayList<Nave>();
	public Hashtable<String,Integer> distancia_naves = new Hashtable<String,Integer>();
	public Hashtable<String,String> color_naves = new Hashtable<String,String>();
	public String nombre;
	public int num_naves = 0;
	
	Carrera(){
		locutor = new Locutor();
		ambulancia = new Ambulancia();
	}
	
	public void llenar_carrera() {
		int opcion;
		
		System.out.print("\033[H\033[2J");
		System.out.flush();
		
		System.out.println("Elija la cantidad de naves a competir: ");
		System.out.println(" [1] Contra una naves \n [2] Contra dos naves");
		opcion = entrada.nextInt();
		
		// Contrincante más usuario.	
		if (opcion == 1) {
			num_naves = 2;
		}
		
		else if (opcion == 2) {
			num_naves = 3;
		}
		
		else {
			System.out.println("ERROR: Selección inválida! \n Inténtelo nuevamente \n");
			llenar_carrera();
		}

		
		for (int i = 0; i<num_naves; i++) { 
			// Se instancian las naves.
			nave = new Nave(); 
			nave.setNombre(i+1);

			nave.ensamble();
			// Se agregan las naves al ArrayList.
			naves.add(nave);
		}
		
		nave.alas.colores();
		locutor.inicio_carrera();
		
		for (Nave nave: naves) {
			// Se mantiene hasta finalizar la carrera.
			while(nave.odometro.getDistancia_final() != 0) {
				nave.calculo();
	
				locutor.desarrollo(nave.getNombre(), nave.getEstado_nave(), nave.getDeterioro(), nave.estanque.getCantidad_combustible());
				nave.setEstado_nave(0);
				
				llenar_recorrido();
								
				// Ambulancia 
				if (nave.getDeterioro() <= 60) {
					System.out.print("La nave " + nave.getNombre() + " esta en reparaciones");
					ambulancia.arreglo_deterioro();
					nave.propulsor.setAceleracion(0);
					nave.estanque.setCantidad_combustible(100);
					nave.setDeterioro(100);
					nave.calculo();
				}
			}
		}
		
		// Fin de la carrera
		if (nave.odometro.getDistancia_final() == 0) {
			locutor.fin_carrera();
			ganar();
		}
		
	}
	
	public static void pausa() {
	    try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
         
        }
	}
	
	public void llenar_recorrido() {
		for (Nave nave: naves) {
			nombre = Integer.toString(nave.getNombre());
			distancia_naves.put("Nave " + nombre, nave.odometro.getDistancia_final());
		}
		
		for (Map.Entry<String, Integer> entrada : distancia_naves.entrySet()) {
			Integer value = entrada.getValue();
 		    value = value/1000;
 		    imprimir_recorrido(value);
 		}
    	 
    }
	
	public void imprimir_recorrido (int distancia) {
		String reset = "\u001B[0m";
		String color = null;
		
		for (Nave nave: naves) {
			if (nave.getColor()!= null) {
				color_naves.put("Nave " + nombre, nave.getColor());
			}
		}
		
		for (Map.Entry<String, String> entry : color_naves.entrySet()) {
			color = entry.getValue();
		}
		
		if (distancia > 0) {
			pausa();
			System.out.print(color + "*" + reset);
			imprimir_recorrido(distancia - 10);
		}
	}
	
	// Definir ganador
	public void ganar() {
		Collections.sort(naves);
		int posiciones = 0;
		int nombre_nave;
		String reset = "\u001B[0m";

		System.out.println("Posiciones:");
		for (Nave nave: naves) {
			posiciones += 1;
			nombre_nave = nave.getNombre();
			System.out.println(" Lugar " + posiciones + "         " + nave.getColor() + "Nave " + nombre_nave + reset );
		}
	}
}
