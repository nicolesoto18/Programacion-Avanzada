import java.util.Random;
import java.util.Scanner;

public class Alas {
	Scanner entrada = new Scanner(System.in); 
	private int color_alas;
	
	public void colores() {
		// Códigos de colores.
		String verde = "\u001B[42m"; 
		String reset = "\u001B[0m";
		String morado = "\u001B[45m";
		String celeste = "\u001B[46m";
		
		// Limpia pantalla.
		System.out.print("\033[H\033[2J"); 
		System.out.flush();
		
		System.out.println("Elija el color de las alas: ");
		System.out.println(celeste +"[1]" + reset + "Celeste");
		System.out.println(verde + "[2]" + reset + "Verde" );
		System.out.println(morado + "[3]" + reset + "Morado");
		
		// Guarda valor de variable.
		color_alas = entrada.nextInt(); 
	}
	
	public void verificar_color() {
		int num_color;
		 Random rand = new Random();
		
			num_color = rand.nextInt((7 - 4) + 1) + 4;
			setColor_alas(num_color);
	}

	// Getters y Setters.
	public int getColor_alas() {
		return color_alas;
	}

	public void setColor_alas(int color_alas) {
		this.color_alas = color_alas;
	}	
}
