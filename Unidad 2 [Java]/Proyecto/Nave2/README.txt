                                                    
						 			    -- SUPER CARRERA N-NIRVA --

La creación de este proyecto esta enfocada en satisfacer al usurio con la versión mucho más sencilla de un viejo juego de nintendo, el cual se espera tener una visión más informatica, sin usar interfaz gráfica pero que por medio de mensajes en pantalla se pueda mostrar al usuario el estado de su nave con los mismos obstaculos que podria tener esta carrera de naves en su realidad. 

    • EMPEZANDO

Al ejecutar el código se mostrará el menú del juego, el cual presentará dos opciones, jugar y salir del juego, para jugar se debe elegir con cuantas naves se desea competir en la carrera, se darán opciones contra una nave y contra dos naves, sin contar la nave del jugador; para diferenciarla de las demás naves competidoras, se deberá elegir un color para las alas de la nave (celeste, verde o morado). Las demas naves con quien se competirá seran aleatoriamente de un color distinto al del jugador, habrá blanco, rojo, azul o amarillo.
La carrera comenzará y se mostrará por pantalla el estado de deterioro en el que se encontrarán cada una de ellas mendiante un locutor, el cual narrará lo que ocurre hasta que todas lleguen a la meta, tambien se podrá conocer el recorrido de cada nave, el cual se representará con un "*" que representa un 10% aprox. de la distancia, luego de esto se ordenaran de manera ascendente las posiciones de las naves para obtener los primeros lugares. 


    • REQUISITOS PREVIOS
     
Sistema operativo Linux
Editor de texto eclipse 
lenguaje programación java con versión mínima de 8.
Instalación de colores en el editor eclipse. 

    • INSTALACIÓN

- Para poder clonar directamente el programa y hacer funcionalidad de este, se debe hacer directamente desde los siguientes link: 
	https://gitlab.com/valegarrido18/VgarridoRepoSoluciones.git o https://gitlab.com/nicolesoto18/NsotoRepoSoluciones.git

-Para instalar jdk se debe utilizar el comando: 

	sudo install openjdk-11-jr 

>>Si se encuentra en ambiente linux, se descarga directamente desde repositorio utilizando dicho comando.
En caso de no estar en la versión 11, se podrá descargar cualquier versión que sea superior a la versión 8. Para ver las versiones disponibles desde su repositorio debe ingresar en terminal el comando:
	
	apt-cache search openjdk jr

Siempre y cuando sea Ubuntu y Debian y luego instalar con el comando:
	
	sudo install openjdk-numeroversion-jr

Para el desarollo del código se utilizará el IDE Eclipse, para descargarlo directamente de terminal debe usar los comandos: 

	wget https://icb.utalca.cl/~fduran/eclipse-java-2019-03-R-linux-gtk-x86_64.tar.gz

	>>Descomprimir utilizando el comando tar:  tar xvf eclipse-java-2019-03-R-linux-gtk-x86_64.tar.gz

-Para instalar los colores se debe ingresar al editor eclipse, luego buscar la pestaña help y se debe clickear en install  new software en donde se abrira una nueva ventana, debe clickear en add el cual abrira otra pestaña nuevamente, en name debe ingresar "color" y en location ingresar el siguiente link:

	https://www.mihai-nita.net/eclipse

Luego debe guargar los cambios cerrando las ventanas y para finalizar y asegurarse de que se hayan guardado los cambios, cierre eclipe y vuelva abrirlo nuevamente.

-- EJECUTANDO LAS PRUEBAS POR TERMINAL --
Para poder entrar al editor eclipse, se debe realizar la ruta de donde se encuentra el ejecutable (eclipse) y luego con el comando ./eclipse se entrara al editor, este puede ser ejecutado desde la version comprimida de todas las carpetas que se podrá encontrar al clonar el repositorio, ingresando la ruta por terminal desde donde se encuentra el comprimido y luego ingresar el comando java -jar proyecto.jar (el nombre proyecto.jar se denomina al nombre del archivo a ejecutar). 


    • CONSTRUIDO CON:
      
Ubuntu: sistema operativo.
Java: lenguaje de programación de propósito general.
Eclipse: editor de texto para la escritura del código de este programa en lenguaje Java.
Pep-8: la narración del código realizado en este proyecto esta basado en las instrucciones dadas por la pep-8.

    • ESPECIFICACIONES DE LA PEP-8 EN EL CÓDIGO: 

- Los tab utilizados en el código están designados por 4 espacios como dice que debe ser dentro de ella. Esto se puede probar dentro de cualquier linea presente en el código.

- Después de cada método se deja un espacios en blanco de separación al final de una y el comienzo de una distinta. Mientras que por cada metodo dentro de la cada función habra solo una línea de separacion-

- Para cada comentario realizado en el código se utiliza de un # para el cual luego debe tener un espacio de separación entre # y la primera palabra escrita en el comentario. 
 
Ejemplo para dar a conocer lo anteriormente escrito pero implementado en el codigo: 

 56         tiempo_actual += odometro.getTiempo();
 57         setTiempo_actual(tiempo_actual);
 58 
 59         System.out.println("");
 60         multiplo_velocidad(velocidad_actual);
 61     }
 62 
 63     public void multiplo_velocidad(int velocidad_actual) { // Por cada multiplo de 5 en la velocidad, hay deterioro.
 64         int resto;
 65         int numero1 = velocidad_actual;
 66         int numero2 = 5;
 67 
 68         resto = numero1 % numero2;
 69 
 70         if (resto == 0) {
 71             deterioro();
 72         }
 73     }



    • VERSIONES
      
Ubuntu 18.04.1 LTS
Ubuntu 17.10 LTS
openjdk version "11.0.3"
openjdk version "1.8.0_171"


    • AUTORES

Valentina Garrido E. >> Desarrollo del script, ejecución de proyecto y narración de README. 
Nicole Soto G. >>Desarrollo del script, ejecución de proyecto y narración de README. 

    • EXPRESIONES DE GRATITUD
Consultas resueltas por, Fabio Duran Verdugo.
Ejemplos: https://gitlab.com/fabioduran/programacion_avanzada.git
